<?php
/**
 * Created by PhpStorm.
 * User: y
 * Date: 05.03.16
 * Time: 13:08
 */

namespace LivetexApi;

/**
 * Class Request
 * @package LivetexApi
 */
abstract class Request
{

    /**
     * @var string
     */
    protected $link = 'http://apiv2.livetex.ru/v2';
    /**
     * @var array
     */
    protected $auth;

    /**
     * @var string
     */
    protected $data;

    /**
     * @var string
     */
    protected $postParams;

    /**
     * @var string
     */
    protected $getParams;

    /**
     * @var bool
     */
    protected $hasGet = false;

    /**
     * @var bool
     */
    protected $hasPost = false;

    /**
     * @var mixed
     */
    public $response;


    /**
     * Request constructor.
     *
     * @param $auth
     * @param $data
     *
     * @throws \Exception
     */
    public function __construct($auth, $data)
    {
        if ( ! function_exists('curl_init')) {
            throw new \Exception('cURL wasn’t installed');
        }
        $this->auth     = $auth;
        $this->data     = $data;
        $this->response = $this->query();
    }


    /**
     * @param $method
     *
     * @return mixed
     * @throws \Exception
     */
    protected function request($method)
    {


        $endpoint = ($this->hasGet) ? sprintf($this->link . '%s?%s', $method,
            $this->getParams) : sprintf($this->link . $method);

        $login    = $this->auth['login'];
        $password = $this->auth['password'];
        $ch       = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        if ($this->hasPost) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postParams);
        }
        $result = curl_exec($ch);
        $error  = curl_error($ch);
        $errno  = curl_errno($ch);
        curl_close($ch);

        if ($error) {
            throw new \Exception($error, $errno);
        }

        return $result;
    }

    /**
     * @param $method_name
     *
     * @return mixed
     */
    protected function getRequest($method_name)
    {
        if ( ! empty($this->data)) {
            $this->hasGet    = true;
            $this->getParams = http_build_query($this->data);
        }
        $result = $this->request($method_name);

        return $result;
    }

    /**
     * @param $method_name
     *
     * @return mixed
     */
    protected function postRequest($method_name)
    {
        if ( ! empty($this->data)) {
            $this->hasPost = true;
            if (is_array($this->data)) {
                $this->postParams = json_encode($this->data);
            } else {
                $this->postParams = $this->data;
            }

        }

        return $this->request($method_name);
    }

    /**
     * @return mixed
     */
    abstract public function query();

}