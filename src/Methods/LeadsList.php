<?php
/**
 * Created by PhpStorm.
 * User: Yriy
 * Date: 05.03.2017
 * Time: 20:13
 */

namespace LivetexApi\Methods;


use LivetexApi\Request;


class LeadsList extends Request
{
    /**
     * @return mixed
     */
    public function query()
    {
        return $this->getRequest('/leads/list');
    }
}