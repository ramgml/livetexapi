<?php
/**
 * Created by PhpStorm.
 * User: Yriy
 * Date: 09.03.2017
 * Time: 21:54
 */

namespace LivetexApi\Methods;

use LivetexApi\Request;

/**
 * Class EmployeesList
 * @package LivetexApi\Methods
 */
class EmployeesList extends Request
{
    /**
     * @return mixed
     */
    public function query()
    {
        return $this->getRequest('/employees/list');
    }
}