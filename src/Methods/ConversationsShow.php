<?php
/**
 * Created by PhpStorm.
 * User: Yriy
 * Date: 05.03.2017
 * Time: 20:06
 */

namespace LivetexApi\Methods;

use LivetexApi\Request;


/**
 * Class ChatList
 * @package LivetexApi\Methods
 */
class  ConversationsShow extends Request
{

    /**
     * @return mixed
     */
    public function query()
    {
        return $this->getRequest('/conversations/show/');
    }
}