<?php
/**
 * Created by PhpStorm.
 * User: y
 * Date: 05.03.17
 * Time: 23:47
 */

namespace LivetexApi;


/**
 * Class Livetex
 * @package LivetexApi
 */
class Livetex
{

    /**
     * @var array
     */
    private $auth = [];

    /**
     * @var
     */
    protected $curl;

    /**
     * ElbaApi constructor.
     *
     * @param $login
     * @param $password
     */
    public function __construct($login, $password)
    {
        $this->auth = array('login' => $login, 'password' => $password);
    }

    /**
     * @param $method
     * @param $data
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call($method, $data)
    {
        $class = '\\LivetexApi\\Methods\\' . ucfirst($method);

        if ( ! class_exists($class)) {
            throw new \Exception('Метод ' . $class . ' не определен');
        }

        $method_obj = new $class($this->auth, $data[0]);
        $result = ($this->isJSON($method_obj->response)) ? json_decode($method_obj->response,
            1) : $method_obj->response;
        if (array_key_exists('retry-after', $result)) {
            sleep($result['retry-after'] + 1);
            $result = $method_obj->query();
        }

        return $result;
    }

    /**
     * @param $string
     *
     * @return bool
     */
    protected function isJSON($string)
    {
        return ((is_string($string) && (is_object(json_decode($string)) || is_array(json_decode($string))))) ? true : false;
    }

}